import React, { Component } from 'react';
import Card from '../Components/Card/Card';
import './style/characters.css';

import Backdrop from '../Components/Backdrop/Backdrop';
import NewCharacterForm from '../Components/NewCharacterForm/NewCharacterForm';

class Characters extends Component {
  // Initialize the state
  constructor(props){
    super(props);
    this.state = {
      formOpen: false,
      list: []
    }
  }

  // Fetch the list on first mount
  componentDidMount() {
    this.getList();
  }

  getList = () => {
    fetch('/api/getList')
    .then(res => res.json())
    .then(list => this.setState({ list }))
  }

  setList = () => {
      fetch('/api/getList')
      .then(res => res.json())
      .then(list => this.state.list)
  }

  updateList = () => {
    this.getList();
  }

  openFormHandler = () =>{
    this.setState((prevState) =>{
      return {formOpen: !prevState.formOpen};
    });
  };

  backdropToggleHandler = () =>{
      this.setState({formOpen: false});
  };

  deleteCharacter = (event) =>{
    var temp = this.state.list;
    event.target.parentElement.remove();
  
    for(var i = 0; i < temp.length; i++)
    {
      if(temp[i].id == event.target.dataset.key)
      {
        temp.splice(i, 1);
        i = temp.length;
      }
    }

    fetch('/api/updateList', {  
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(temp)
    });
  }


  render() {
    let backdrop;
    let form;
    const { list } = this.state;

    if(this.state.formOpen){
      backdrop = <Backdrop click={this.backdropToggleHandler} />
      form = <NewCharacterForm updateList={this.updateList} click={this.backdropToggleHandler} />
    }

    return (
      <div className="characters">
        {backdrop}
        {form}
        <input className="search" type='text' placeholder="Search"></input>
        {list.length ? (
          <div className="character-container">
            {list.map((item) => {
              return(
                  <div className='character'>
                  <Card key={item.id} name={item.name} class={item.class} />
                  <div data-key={item.id} onClick={this.deleteCharacter} className="remove-button">X</div>
                  </div>
              );
                })}
          </div>
        ) : (
          <div>
            <h2>No Characters Found</h2>
          </div>
        )}
          <input onClick={this.openFormHandler} className="add" type="submit" value="+"></input>
  
      </div>
    );
  }
}

export default Characters;