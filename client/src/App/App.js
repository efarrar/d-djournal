import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Toolbar from './Components/Toolbar/Toolbar.js';
import SideDrawer from './Components/SideDrawer/SideDrawer';
import Backdrop from './Components/Backdrop/Backdrop';

// Pages
import Home from './pages/Home';
import Campaigns from './pages/Campaigns';
import Characters from './pages/Characters';
import CharacterSheet5e from './pages/CharacterSheet5e';


class App extends Component {

  state = {
      sideDrawerOpen: false,
    }


  drawerToggleHandler = () =>{
    this.setState((prevState) =>{
      return {sideDrawerOpen: !prevState.sideDrawerOpen};
    });
  };

  backdropToggleHandler = () =>{
      this.setState({sideDrawerOpen: false});
  };

  render() {
    let backdrop;

    const App = () => (
      <div>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/campaigns' component={Campaigns}/>
          <Route path='/characters' component={Characters}/>
          <Route path='/charactersheet5e' component={CharacterSheet5e}/>
        </Switch>
      </div>
    )

    if(this.state.sideDrawerOpen){
      backdrop = <Backdrop click={this.backdropToggleHandler} />
    }

    return (
      <div className="App" style={{height: '100%'}}>
        {backdrop}
        <Toolbar drawerToggleHandler={this.drawerToggleHandler} />
        <SideDrawer show={this.state.sideDrawerOpen} />
        <Switch>
          <App/>
        </Switch>
      </div>
    );
  }
}

export default App;
