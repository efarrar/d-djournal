import React from 'react';
import './NewCharacterForm.css';
import characterData from '../../Data/characterData.json';


class newCharacterForm extends React.Component {

    state = {
        character: {
            id: '',
            name: '',
            class: '',
        },
        list: [],
    }

    componentDidMount() {
        this.getList();
    }
    
    getList = () => {
        fetch('/api/getList')
        .then(res => res.json())
        .then(list => this.setState({ list }))
    }

    createNewCharacter = () =>{
        var data  = {
            id:this.state.list.length,
            name:this.state.name,
            class:this.state.class
        };

        this.state.list.push(data);

        fetch('/api/updateList', {  
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.list)
        });

        this.props.updateList();
        this.props.click();
    };

    updateName = (event) =>{
        this.setState({name : event.target.value})

    }

    updateClass = (event) =>{
        this.setState({class : event.target.value})
    }

    render(){

        return (
            <div className="newCharacterForm">
                <input placeholder="Character Name" className='character_name' onChange={this.updateName}></input>
                <input placeholder="Character Level & Class" className='class_name' onChange={this.updateClass}></input>
                <button className='new_character_button' onClick={this.createNewCharacter} >
                    Submit
                </button>
            </div>
        );
    }

};

export default newCharacterForm;