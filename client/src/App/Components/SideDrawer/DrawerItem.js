import React from 'react';


const drawerItem = props => (
        <div>
            <div className="nav_button" onClick={props.click}>
               {props.button}
            </div>
        </div>
)

export default drawerItem;