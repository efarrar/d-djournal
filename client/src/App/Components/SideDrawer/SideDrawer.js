import React from 'react';
import './SideDrawer.css';
import DrawerItem from './DrawerItem';
import { Link } from 'react-router-dom';

const sideDrawer = props => {
    let drawerClasses = 'side_drawer';

    if(props.show){
        drawerClasses = 'side_drawer open';
    }
    
    return (
        <div className={drawerClasses}>

            <Link to={'/'} style={{ textDecoration: 'none' }}>
                <DrawerItem button={'Home'}/>
            </Link>

            <Link to={'/campaigns'} style={{ textDecoration: 'none' }}>
                <DrawerItem button={'Campaigns'}/>
            </Link>

            <Link to={'/characters'} style={{ textDecoration: 'none' }}>
                <DrawerItem button={'Characters'}/>
            </Link>
            <DrawerItem button={'NPCs'}/>
            <DrawerItem button={'Contact'}/>

        </div>
    );
};

export default sideDrawer;