import React from 'react';
import './Card.css';
import { Link } from 'react-router-dom';

const card = props => {
    
    return (
        <div className="card">
        
            <Link to={'/charactersheet5e'} style={{ textDecoration: 'none' }}>
             <img src={require('./temp.png')} alt={require('./temp.png')} className="image"/>
                <div className="container">
                    <h4><b>{props.name}</b></h4> 
                    <p>{props.class}</p> 
            </div>
            </Link>

        </div>
    );
};

export default card;