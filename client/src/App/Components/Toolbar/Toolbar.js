import React from 'react';
import DrawerToggleButton from '../SideDrawer/DrawerToggleButton'; 
import './Toolbar.css';

const toolbar = props => (
    <header className="toolbar">
        <nav className="toolbar_navigation">
            <div>
                <DrawerToggleButton click={props.drawerToggleHandler} />
            </div>
            <div className="toolbar_logo "><a href="/">D&DJournal</a></div>
            <div className="toolbar_spacer"></div>
            <div className="toolbar_list">
                <ul>
                    <li>
                        <a href="/">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
)

export default toolbar;