const express = require('express');
const path = require('path');
var list = require('./client/src/App/Data/characterData.json');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser')


// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// An api endpoint that returns a short list of items
app.get('/api/getList', (req,res) => {
    res.json(list);
    console.log('Sent list of items');
});

app.post('/api/updateList', (req,res) => {
    console.log(req.body);

    updateList(req.body, function(err) {
        if (err) {
          res.status(404).send('Character not saved');
          return;
        }
      });
});


// Handles any requests that don't match the ones above
app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname+'/index.js'));
});

function updateList(newList, callback) {
    list = newList;
    fs.writeFile('./client/src/App/Data/characterData.json', JSON.stringify(list), callback);
}

const port = process.env.PORT || 5000;
app.listen(port);

console.log('App is listening on port ' + port);